// 维护需要提供长连接服务的客户信息
var consumers = {
    "leiketang": {
        appId: "leiketang", // 标志唯一的一个服务，不可重复
        appName: "雷课堂",
        appKey: "", // 用来做权限验证
        callbackUrl: {
            hostname: "itest.leiketang.cn",
            path: "/websocket"
        }, // 当有客户端连接时，将客户端id发送给此url 方式 post "callbackUrl" body:clientId=id
        clients: {}, // 记录连接到此app的客户端id
        channels: {
            "public": []
        } // 记录此app的频道和频道中的客户端id列表
    }
};

module.exports = consumers;
