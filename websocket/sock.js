var sockjs = require('sockjs');
var http = require('http');
var https = require('https');
var fs = require('fs');
var privateKey = fs.readFileSync('./path/to/Apache/3_www.wsservice.cn.key', 'utf8');
var certificate = fs.readFileSync('./path/to/Apache/2_www.wsservice.cn.crt', 'utf8');
var credentials = {
    key: privateKey,
    cert: certificate
};

var consumers = require('../websocket/consumer');

var wsService = sockjs.createServer({
    sockjs_url: 'https://cdn.bootcss.com/sockjs-client/1.1.4/sockjs.min.js'
});
var clients = [];
wsService.on('connection', function(conn) {
    console.log('connection' + conn);
    clients[conn.id] = conn;

    var appId = getParam(conn.url, "appId");
    var channel = getParam(conn.url, "channel");
    conn.appId = appId;
    conn.channel = channel;
    if (appId && consumers[appId]) {
        consumers[appId].clients[conn.id] = conn.id;
        if (channel) {
            if (consumers[appId].channels[channel]) {
                consumers[appId].channels[channel].push(conn.id);
            } else {
                consumers[appId].channels[channel] = [];
                consumers[appId].channels[channel].push(conn.id);
            }
        }
    }

    conn.on('data', function(message) {
        // conn.write(message);
        for (i in clients) {
            clients[i].write(message);
        }
        console.log('message ' + conn, message);
    });
    conn.on('close', function() {
        var id = this.id;
        var appId = this.appId;
        var channel = this.channel;

        // 清楚内存中维护的此客户端数据
        delete clients[id];
        if (appId) {
            delete consumers[appId].clients[id];
            if (channel) {
                for (var i = 0; i < consumers[appId].channels[channel].length; i++) {
                    var clientId = consumers[appId].channels[channel][i];
                    if (clientId == id) {
                        consumers[appId].channels[channel].splice(i, 1);
                        break;
                    }
                }
                if (consumers[appId].channels[channel].length == 0) {
                    delete consumers[appId].channels[channel];
                }
            }
        }

        console.log('close ' + conn);
    });
});

var server = http.createServer();
wsService.installHandlers(server, {
    prefix: '/wsService'
});
server.listen(3002, '0.0.0.0');

var httpsServer = https.createServer(credentials);
wsService.installHandlers(httpsServer, {
    prefix: '/wssService'
});
httpsServer.listen(3003, '0.0.0.0');

wsService.sendMessage = function(clientId, message) {
    var client = clients[clientId];
    if (client) {
        client.write(message);
    }
}

function getParam(url, name) {
    url = url.substring(url.indexOf('?'));
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = url.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]);
    return null;
}

module.exports = wsService;
