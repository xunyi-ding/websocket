var express = require('express');
var router = express.Router();

var request = require('request');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'Express'
    });
});

/* GET home page. */
router.get('/test', function(req, res, next) {
    request('http://www.baidu.com', function(error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body) // 打印google首页
        }
    })
    res.send('respond with a resource');

    // request({
    //     url: url,
    //     method: "POST",
    //     json: true,
    //     headers: {
    //         "content-type": "application/json",
    //     },
    //     body: JSON.stringify(requestData)
    // }, function(error, response, body) {
    //     if (!error && response.statusCode == 200) {}
    // });
});

module.exports = router;
