var express = require('express');
var router = express.Router();

var consumers = require('../websocket/consumer');

// 获取全部注册服务的信息
router.get('/', function(req, res, next) {
    res.json(consumers);
});
// 注册服务
router.post('/', function(req, res, next) {
    // var service = {
    //     appId: "leiketang", // 标志唯一的一个服务，不可重复
    //     appName: "雷课堂",
    //     appKey: "", // 用来做权限验证
    //     callbackUrl: {
    //         hostname: "itest.leiketang.cn",
    //         path: "/websocket"
    //     } // 当有客户端连接时，将客户端id发送给此url 方式 post "callbackUrl" body:clientId=id
    // };
    var service = req.body;
    service.clients = {};
    if (consumers[service.appId] && consumers[service.appId].clients) {
        service.clients = consumers[service.appId].clients;
    }
    service.channels = {};
    if (consumers[service.appId] && consumers[service.appId].channels) {
        service.channels = consumers[service.appId].channels;
    }
    if (service.appId && service.appName) {
        // 最多为10个项目提供服务
        if (countProperty(consumers) > 10) {
            var err = new Error('too many service');
            err.status = 500;
            next(err);
        } else {
            consumers[service.appId] = service;
            res.json("register success");
        }
    } else {
        var err = new Error('missing parameter');
        err.status = 400;
        next(err);
    }
});
// 获取单个注册服务信息
router.get('/:appId', function(req, res, next) {
    var appId = req.params.appId;
    if (appId) {
        res.json(consumers[appId]);
    } else {
        var err = new Error(appId + ' does not exist.');
        err.status = 400;
        next(err);
    }
});
// 删除一个服务
router.delete('/:appId', function(req, res, next) {
    var appId = req.params.appId;
    if (appId) {
        delete consumers[appId];
        res.json("delete success");
    } else {
        var err = new Error(appId + ' does not exist.');
        err.status = 400;
        next(err);
    }
});
// 获取连接到一个服务上的客户端id列表
router.get('/:appId/client', function(req, res, next) {
    var appId = req.params.appId;
    if (appId) {
        res.json(consumers[appId].clients);
    } else {
        var err = new Error(appId + ' does not exist.');
        err.status = 400;
        next(err);
    }
});
// 获取连接到一个服务上的channel列表
router.get('/:appId/channel', function(req, res, next) {
    var appId = req.params.appId;
    if (appId) {
        res.json(consumers[appId].channels);
    } else {
        var err = new Error(appId + ' does not exist.');
        err.status = 400;
        next(err);
    }
});

function countProperty(obj) {
    var count = 0;
    for (var i in obj)
        if (obj.hasOwnProperty(i)) {
            count++;
        }
    return count;
}

module.exports = router;
