var express = require('express');
var router = express.Router();

var consumers = require('../websocket/consumer');
var websocket = require('../websocket/sock');

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.json('respond with a resource');
});
// 发送消息给单个客户端
router.post('/sendToClient', function(req, res, next) {
    var appId = req.query.appId;
    var clientId = req.query.clientId;
    var message = req.body;
    console.log(message);
    try {
        message = JSON.stringify(message);
    } catch (e) {
        message = "Illegal JSON";
    }
    if (appId && clientId && consumers[appId]) {
        websocket.sendMessage(clientId, message);
        res.json("send success");
    } else {
        var err = new Error('send failed');
        err.status = 400;
        next(err);
    }
});
// 发送消息给一个频道
router.post('/sendToChannel', function(req, res, next) {
    var appId = req.query.appId;
    var channel = req.query.channel;
    var message = req.body;
    console.log(message);
    try {
        message = JSON.stringify(message);
    } catch (e) {
        message = "Illegal JSON";
    }
    if (appId && channel && consumers[appId] && consumers[appId].channels[channel]) {
        var clientIdList = consumers[appId].channels[channel];
        for (c in clientIdList) {
            var clientId = clientIdList[c];
            websocket.sendMessage(clientId, message);
        }
        res.json("send success");
    } else {
        var err = new Error('send failed');
        err.status = 400;
        next(err);
    }
});
// 发送消息给所有此appId下的客户端
router.post('/sendToAll', function(req, res, next) {
    var appId = req.query.appId;
    var message = req.body;
    console.log(message);
    try {
        message = JSON.stringify(message);
    } catch (e) {
        message = "Illegal JSON";
    }

    if (appId && consumers[appId]) {
        var clientIdList = consumers[appId].clients;
        for (c in clientIdList) {
            var clientId = clientIdList[c];
            websocket.sendMessage(clientId, message);
        }
        res.json("send success");
    } else {
        var err = new Error('send failed');
        err.status = 400;
        next(err);
    }
});

module.exports = router;
