var redbird = new require('redbird')({
    port: 80,
    secure: false,
    // Specify filenames to default SSL certificates (in case SNI is not supported by the
    // user's browser)
    ssl: {
        port: 443,
        key: "./path/to/Apache/3_www.wsservice.cn.key",
        cert: "./path/to/Apache/2_www.wsservice.cn.crt",
    }
});

// Since we will only have one https host, we dont need to specify additional certificates.
redbird.register('http://www.wsservice.cn', 'http://10.186.49.123:3000');

redbird.register('https://www.wsservice.cn', 'https://10.186.49.123:3001');
