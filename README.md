# WebsocketService

这是一个用来提供websocket长连接服务的平台，其他后台服务器可以调用接口来注册自己的服务，从而使用此平台提供的长连接服务，浏览器端直接和此平台连接，所以自己的后台只需要提供一个callback接口，并在需要向客户端推送数据时调用此平台的发送消息接口即可。

## 基于

- [expressjs / express](https://github.com/expressjs/express)
- [sockjs / sockjs-node](https://github.com/sockjs/sockjs-node)

## 使用方式

在项目根目录下运行

```
npm install
```

来安装依赖，运行

```
npm start
```

来启动服务器端，默认会运行在`localhost:3000`端口，可根据需要修改。默认的`index.html`中，使用的是客户端向服务器端发送消息时广播到所有其他连接到平台的客户端的功能。

## 接入服务方式

对于需要使用此平台提供的websocket服务的其他后台服务器，首先需要调用

```javascript
post /service
{
    appId: "test", // 标志唯一的一个服务，不可重复
    appName: "测试服务",
    appKey: "", // 用来做权限验证
    callbackUrl: {
        hostname: "api.leiketang.cn",
        path: "/courses/532"
    } // 当有客户端连接时，将客户端id发送给此url 方式 get "callbackUrl?clientId=id"
}
```

接口来注册自己的服务，注册成功之后，在需要使用长连接服务的客户端建立长连接时需要在查询参数中传递自己对应后台服务器的appId，和一个可以唯一标志自己客户端的id（例如时间戳）方式为

```javascript
    <script src="https://cdn.bootcss.com/sockjs-client/1.1.4/sockjs.min.js"></script>

    var sockjs_url = location.protocol + "//" + location.hostname + ":3002" +
        "/wsService?appId=" + appId + "&channel=" + channel;
    sockjs = new SockJS(sockjs_url);
```

此时平台成功于客户端建立长连接后，会调用服务器注册时提供的callbackUrl，并以查询参数的方式传递此客户端的clientId，服务器需要自己记录此clientId以用于对单独的客户端推送消息。 当服务器需要向客户端推送消息时，只需调用平台的对应接口

```
post /sendToClient?id=111&clientId=cd81f808-e504-44fa-a485-0ad7f8a0a6c1asdfsfd
post /sendToChannel?appId=222&channel=public
post /sendToAll?appId=222
```

send接口用于向对应id的单独客户端发送消息，sendToAll用于向此连接于此服务appId的所有客户端推送消息。平台不会对服务器发送的信息做处理，调用send接口时body中发送的信息会被转为json字符串然后推送给客户端。

## 其他接口

```
get /service // 获取全部注册服务信息
get /service/:appId // 获取单个注册服务信息
delete /service/:appId // 删除一个服务
get /service/:appId/client // 获取连接到一个服务上的客户端id列表
get /service/:appId/channel// 获取此服务的channel列表
```

## 使用端口

- 静态页面和rest接口使用3001端口(https)和3000端口(http)
- sockjs搭建的长连接服务使用3002端口，使用ws(http)协议
- sockjs搭建的长连接服务使用3003端口，使用wss(https)协议
