$(document).ready(function() {
    var name;
    var sockjs;

    var div = $(".text-area");

    var print = function(m, p) {
        p = (p === undefined) ? "" : JSON.stringify(p);
        div.prepend($("<br>"));
        div.prepend($("<code>").text(m + " " + p));
    };

    $("#appId").val("leiketang");
    $("#channel").val("public");
    $("#enterBtn").click(function() {
        name = $("#name").val();
        var appId = $("#appId").val();
        var channel = $("#channel").val();

        var sockjs_url = location.protocol + "//" + location.hostname + ":3002" + "/wsService?appId=" + appId + "&channel=" + channel;
        if (location.protocol == "https:") {
            sockjs_url = location.protocol + "//" + location.hostname + ":3003" + "/wssService?appId=" + appId + "&channel=" + channel;
        }
        sockjs = new SockJS(sockjs_url);

        sockjs.onopen = function() {
            print("[*] 进入聊天室", name);
        };
        sockjs.onmessage = function(e) {
            var message, data;
            try {
                data = JSON.parse(e.data);
            } catch (e) {
                data = {};
            }

            message = data.name + "(" + data.time + "):" + data.message;
            print("[.] 消息", message);
        };
        sockjs.onclose = function() {
            print("[*] 退出聊天室");
        };

        $("#dataForm").hide();
        $("#messageForm").show();
    });

    $("#quitBtn").click(function() {
        $("#messageForm").hide();
        $("#dataForm").show();
    });

    $("#sendBtn").click(function() {
        var message = $("#message").val();
        $("#message").val("");

        var data = {};
        data.name = name;
        data.message = message;
        var now = new Date();
        data.time = now.format("hh:mm:ss");

        sockjs.send(JSON.stringify(data));
    });
});
